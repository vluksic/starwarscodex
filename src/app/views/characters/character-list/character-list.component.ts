import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { CharactersService } from '../characters.service';
import { CharactersResponse } from '../../../shared/Characters-response';
import { URL } from '../../../shared/Types';
import { PersistenceService } from '../../../shared/persistence/persistence.service';
import { DataType } from '../../../shared/persistence/Data-type.enum';

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.scss']
})
export class CharacterListComponent implements OnInit, OnDestroy {
  private subscriptions: Array<Subscription> = [];
  private charactersSub: Subscription | undefined;

  characters: CharactersResponse = {} as CharactersResponse;
  characters$: Observable<CharactersResponse> | undefined;

  characterId = 0;

  searchedQuery = '';
  query = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private charactersService: CharactersService,
    private persistenceService: PersistenceService
    ) { }

  ngOnInit(): void {
    this.characters$ = this.route.paramMap.pipe(
      switchMap(params => this.charactersService.getCharactersObservable())
    );

    this.charactersSub = this.characters$.subscribe((result: CharactersResponse) => {
        this.characters = result;
    });

    this.subscriptions.push(this.charactersSub);

    const persistedSearchQuery = this.persistenceService.getFromLocalStorage(DataType.searchQuery);
    if (persistedSearchQuery !== null) { this.searchedQuery = JSON.parse(persistedSearchQuery); }
  }

  gotoCharacter(url: URL): void {
    const characterId = url.match(/[0-9]+/);
    if (characterId) { this.characterId = +characterId[0]; }
    this.router.navigate(['/character', this.characterId]);
  }

  valueChange(value: string): void {
    this.query = value;
  }

  @HostListener('window:beforeunload')
  ngOnDestroy(): void {
      this.subscriptions?.forEach(subscription => {
        subscription.unsubscribe();
      });

      this.persistenceService.saveToLocalStorage(DataType.searchQuery, this.searchedQuery);
    }
}
