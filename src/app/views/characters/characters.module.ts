import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CharactersRoutingModule } from './characters-routing.module';
import { CharacterListComponent } from './character-list/character-list.component';
import { CharacterDetailsComponent } from './character-details/character-details.component';
import { MatButtonModule } from '@angular/material/button';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';


@NgModule({
  declarations: [
    CharacterListComponent,
    CharacterDetailsComponent
  ],
    imports: [
        CommonModule,
        CharactersRoutingModule,
        MatButtonModule,
        SharedModule,
        FormsModule,
        MatInputModule
    ]
})
export class CharactersModule { }
