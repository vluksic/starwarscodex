import { Injectable } from '@angular/core';
import { DataService } from 'src/app/shared/data-service/data.service';
import { Observable } from 'rxjs';
import { CharactersResponse } from '../../shared/Characters-response';
import { Character } from '../../shared/Character';
import { PersistenceService } from '../../shared/persistence/persistence.service';
import { DataType } from '../../shared/persistence/Data-type.enum';

@Injectable({
    providedIn: 'root'
  })
export class CharactersService {

  constructor(
      private dataService: DataService,
      private persistenceService: PersistenceService
  ) { }

  public setCharacterDetails(id: number): void {
    return this.dataService.setCharactersPageById(id);
  }

  public getCharacterDetailsObservable(): Observable<Character> {
    return this.dataService.getCharacterDetailsObservable();
  }

  public getCharactersObservable(): Observable<CharactersResponse> {
    return this.dataService.getCharactersObservable();
  }

  public favouriteToggle(isFavourite: boolean, characterId: number): void {
    if (isFavourite) {
      this.addFavouriteToStorage(characterId);
    } else {
      this.removeFavouriteFromStorage(characterId);
    }
  }

  private addFavouriteToStorage(characterId: number): void {
    const persistedFavourites = this.persistenceService.getFromLocalStorage(DataType.favourites);
    let favourites;
    if (persistedFavourites !== null) {
      favourites = JSON.parse(persistedFavourites);
      favourites.push(characterId);
    } else {
      favourites = [characterId];
    }
    this.persistenceService.saveToLocalStorage(DataType.favourites, favourites);
  }

  private removeFavouriteFromStorage(characterId: number): void {
    const persistedFavourites = this.persistenceService.getFromLocalStorage(DataType.favourites);
    if (persistedFavourites !== null) {
      const favourites = JSON.parse(persistedFavourites);
      const indexToDelete = favourites.findIndex((character: number) => character === characterId);
      favourites.splice(indexToDelete, 1);
      this.persistenceService.saveToLocalStorage(DataType.favourites, favourites);
    }
  }
}
