import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { CharactersService } from '../characters.service';
import { switchMap } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { Character } from '../../../shared/Character';
import { DataType } from '../../../shared/persistence/Data-type.enum';
import { PersistenceService } from '../../../shared/persistence/persistence.service';
import { URL } from '../../../shared/Types';

@Component({
  selector: 'app-character-details',
  templateUrl: './character-details.component.html',
  styleUrls: ['./character-details.component.scss']
})
export class CharacterDetailsComponent implements OnInit, OnDestroy {

  private subscriptions: Array<Subscription> = [];
  private characterDetailsSub: Subscription | undefined;

  character: Character = {} as Character;
  character$: Observable<Character> | undefined;
  characterId = 0;

  isFavourite = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private charactersService: CharactersService,
    private persistenceService: PersistenceService
  ) { }

  ngOnInit(): void {
    this.character$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        const id = params.get('id');
        if (id) { this.characterId = +id; }
        this.charactersService.setCharacterDetails(this.characterId);
        return this.charactersService.getCharacterDetailsObservable();
      }));

    this.characterDetailsSub = this.character$.subscribe((result: Character) => {
        this.character = result;
    });
    this.subscriptions.push(this.characterDetailsSub);

    this.checkIfCharacterIsFavourite();
  }

  toggleFavourite(): void {
    this.isFavourite = !this.isFavourite;
    this.charactersService.favouriteToggle(this.isFavourite, this.characterId);
  }

  checkIfCharacterIsFavourite(): void {
    const persistedFavourites = this.persistenceService.getFromLocalStorage(DataType.favourites);
    if (persistedFavourites !== null) {
      const favourites = JSON.parse(persistedFavourites);
      this.isFavourite = !!favourites.find((id: any) => id === this.characterId);
    }
  }

  gotoFilm(url: URL): void {
    const filmId = url.match(/[0-9]+/);
    if (filmId) { this.router.navigate(['/film', +filmId[0]]); }
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
