import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { FilmsService } from '../films.service';
import { Observable, Subscription } from 'rxjs';
import { Film } from '../../../shared/Film';
import { URL } from '../../../shared/Types';

@Component({
  selector: 'app-film-details',
  templateUrl: './film-details.component.html',
  styleUrls: ['./film-details.component.scss']
})
export class FilmDetailsComponent implements OnInit, OnDestroy {

  private subscriptions: Array<Subscription> = [];
  private filmDetailsSub: Subscription | undefined;

  film: Film = {} as Film;
  film$: Observable<Film> | undefined;
  filmId = 0;

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private filmsService: FilmsService
  ) { }

  ngOnInit(): void {
    this.film$ = this.route.paramMap.pipe(
        switchMap((params: ParamMap) => {
          const id = params.get('id');
          if (id) { this.filmId = +id; }
          this.filmsService.setFilmDetails(this.filmId);
          return this.filmsService.getFilmDetailsObservable();
        }));

    this.filmDetailsSub = this.film$.subscribe((result: Film) => {
      this.film = result;
    });
    this.subscriptions.push(this.filmDetailsSub);
  }

  gotoCharacter(url: URL): void {
    const characterId = url.match(/[0-9]+/);
    if (characterId) { this.router.navigate(['/character', +characterId[0]]); }
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

}
