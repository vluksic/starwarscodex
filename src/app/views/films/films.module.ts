import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FilmsRoutingModule } from './films-routing.module';
import { FilmDetailsComponent } from './film-details/film-details.component';
import { FilmListComponent } from './film-list/film-list.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';


@NgModule({
  declarations: [
    FilmDetailsComponent,
    FilmListComponent
  ],
    imports: [
        CommonModule,
        FilmsRoutingModule,
        SharedModule,
        FormsModule,
        MatInputModule
    ]
})
export class FilmsModule { }
