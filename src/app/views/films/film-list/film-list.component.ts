import { Component, OnDestroy, OnInit } from '@angular/core';
import { switchMap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { FilmsResponse } from '../../../shared/Films-response';
import { URL } from '../../../shared/Types';
import { FilmsService } from '../films.service';

@Component({
  selector: 'app-film-list',
  templateUrl: './film-list.component.html',
  styleUrls: ['./film-list.component.scss']
})
export class FilmListComponent implements OnInit, OnDestroy {
  private filmsSub: Subscription | undefined;
  private subscriptions: Array<Subscription> = [];

  films: FilmsResponse = {} as FilmsResponse;
  films$: Observable<FilmsResponse> | undefined;

  filmId = 0;
  searchedQuery = '';
  query = '';

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private filmsService: FilmsService
  ) { }

  ngOnInit(): void {
    this.filmsService.getInitialFilms();
    this.films$ = this.route.paramMap.pipe(
        switchMap(params => this.filmsService.getFilmsObservable())
    );

    this.filmsSub = this.films$.subscribe((result: FilmsResponse) => {
      this.films = result;
    });

    this.subscriptions.push(this.filmsSub);
  }

  gotoFilm(url: URL): void {
    const filmId = url.match(/[0-9]+/);
    if (filmId) { this.filmId = +filmId[0]; }
    this.router.navigate(['/film', this.filmId]);
  }

  valueChange(value: string): void {
    this.query = value;
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

}
