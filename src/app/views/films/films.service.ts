import { Injectable } from '@angular/core';
import { DataService } from '../../shared/data-service/data.service';
import { Observable } from 'rxjs';
import { FilmsResponse } from '../../shared/Films-response';
import { Film } from '../../shared/Film';

@Injectable({
  providedIn: 'root'
})
export class FilmsService {

  constructor(
      private dataService: DataService,
  ) { }

  public getInitialFilms(): void {
    this.dataService.getInitialFilms();
  }

  public setFilmDetails(id: number): void {
    return this.dataService.setFilmsPageById(id);
  }

  public getFilmDetailsObservable(): Observable<Film> {
    return this.dataService.getFilmDetailsObservable();
  }

  public getFilmsObservable(): Observable<FilmsResponse> {
    return this.dataService.getFilmsObservable();
  }
}
