import { Component, Input, OnInit, Output, EventEmitter, OnChanges } from '@angular/core';
import { PaginationService } from './pagination.service';
import { URL } from '../Types';
import { PersistenceService } from '../persistence/persistence.service';
import { DataType } from '../persistence/Data-type.enum';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit, OnChanges {

  @Input() previousPage: URL = '';
  @Input() nextPage: URL = '';
  @Input() totalItems = 0;
  @Input() showingItems = 0;
  @Output() pageChange = new EventEmitter<URL>();

  pageNo = 1;
  totalPages = 1;


  constructor(
      private paginationService: PaginationService,
      private persistenceService: PersistenceService
  ) { }

  ngOnInit(): void {
    const persistedPage = this.persistenceService.getFromLocalStorage(DataType.page);
    persistedPage !== null ? this.gotoPage(JSON.parse(persistedPage)) : this.paginationService.gotoDefaultPage();
  }

  gotoPage(url: URL): void {
    if (!url) { return; }
    const page = url.match(/[0-9]+/);
    if (page) { this.pageNo = +page[0]; }
    this.pageChange.emit(url);
    this.persistenceService.saveToLocalStorage(DataType.page, url);
    this.paginationService.gotoPage(url);
  }

  ngOnChanges(changes: any): void {
    this.totalPages = (Math.floor(this.totalItems / this.pageNo) !== this.totalPages) ?
        (this.totalItems % this.showingItems !== 0 ? Math.ceil(this.totalItems / this.showingItems) : this.pageNo)
        : this.pageNo;
  }

}
