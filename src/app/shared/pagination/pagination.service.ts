import { Injectable } from '@angular/core';
import { DataService } from '../data-service/data.service';
import { URL } from '../Types';

@Injectable({
  providedIn: 'root'
})
export class PaginationService {

  constructor(private dataService: DataService) { }

  gotoPage(page: URL): void {
    if (!page) { return; }
    this.dataService.setCharactersPage(page); // TODO refactor to be unspecific
  }

  gotoDefaultPage(): void {
    this.dataService.getInitialCharacters();
  }
}
