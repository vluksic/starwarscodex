import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationComponent } from './pagination.component';
import { MatButtonModule } from '@angular/material/button';



@NgModule({
    declarations: [PaginationComponent],
    exports: [
        PaginationComponent
    ],
    imports: [
        CommonModule,
        MatButtonModule
    ]
})
export class PaginationModule { }
