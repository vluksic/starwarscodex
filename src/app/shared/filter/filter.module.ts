import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterComponent } from './filter.component';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';



@NgModule({
    declarations: [FilterComponent],
    exports: [
        FilterComponent
    ],
    imports: [
        CommonModule,
        MatInputModule,
        FormsModule
    ]
})
export class FilterModule { }
