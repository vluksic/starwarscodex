import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  @Input() searchedQuery = '';
  @Input() label = 'Enter text';
  @Output() valueChange = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  handleChange(event: any): void {
    if (event.type === 'change') {
      this.valueChange.emit(event.target.value);
    }
  }

}
