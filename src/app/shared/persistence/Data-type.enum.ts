export enum DataType {
    searchQuery = 'searchQuery',
    page = 'page',
    favourites = 'favourites'
}
