import { Injectable } from '@angular/core';
import { DataType } from './Data-type.enum';

@Injectable()
export class PersistenceService {

  public saveToLocalStorage(type: DataType, data: string | null): void {
    if (!data) { return; }
    localStorage.setItem(type, JSON.stringify(data));
  }

  public getFromLocalStorage(type: DataType): string | null {
    return localStorage.getItem(type);
  }
}
