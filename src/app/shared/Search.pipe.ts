import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'search'
})
export class SearchPipe implements PipeTransform {
    public transform(value: any, key: string, term: string): any {
        if (!term) { return value; }
        return (value || []).filter((item: any) => new RegExp(term, 'gi').test(item[key]));
    }
}
