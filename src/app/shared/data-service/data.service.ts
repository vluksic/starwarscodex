import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { CharactersResponse } from '../Characters-response';
import { URL } from '../Types';
import { Character } from '../Character';
import { FilmsResponse } from '../Films-response';
import { Film } from '../Film';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private SWAPI_BASE_URL = '/api';
  public charactersList = new BehaviorSubject<CharactersResponse>({} as CharactersResponse);
  public characterDetails = new Subject<Character>();
  public filmsList = new BehaviorSubject<FilmsResponse>({} as FilmsResponse);
  public filmDetails = new Subject<Film>();

  constructor(private http: HttpClient) {}

  private _getCharacters(): Observable<any> {
    return this.http.get(`${this.SWAPI_BASE_URL}/people`);
  }

  private _getCharacterById(id: number): Observable<any> {
    return this.http.get(`${this.SWAPI_BASE_URL}/people/${id}`);
  }

  public getInitialCharacters(): void {
    this._getCharacters().subscribe((result: CharactersResponse) => this.setCharactersValue(result));
  }

  public getCharactersObservable(): Observable<CharactersResponse> {
    return this.charactersList.asObservable();
  }

  public getCharactersValue(): CharactersResponse {
    return this.charactersList.getValue();
  }

  public setCharactersValue(value: CharactersResponse): any {
    return this.charactersList.next(value);
  }

  public setCharactersPage(url: URL): void {
    this.getPage(url).subscribe((result: any) => {
      if (!!result.count) {
        this.setCharactersValue(result);
      } else {
        this.setCharacterDetailsValue(result);
      }
    });
  }

  public setCharactersPageById(id: number): void {
    this._getCharacterById(id).subscribe((result: Character) => {
      this.setCharacterDetailsValue(result);
    });
  }

  public getCharacterDetailsObservable(): Observable<Character> {
    return this.characterDetails.asObservable();
  }

  public setCharacterDetailsValue(value: Character): any {
    return this.characterDetails.next(value);
  }

  public setCharacterDetailsPage(url: URL): void {
    this.getPage(url).subscribe((result: Character) => {
      this.setCharacterDetailsValue(result);
    });
  }


  private _getFilms(): Observable<any> {
    return this.http.get(`${this.SWAPI_BASE_URL}/films`);
  }

  private _getFilmById(id: number): Observable<any> {
    return this.http.get(`${this.SWAPI_BASE_URL}/films/${id}`);
  }

  public getInitialFilms(): void {
    this._getFilms().subscribe((result: FilmsResponse) => this.setFilmsValue(result));
  }

  public getFilmsObservable(): Observable<FilmsResponse> {
    return this.filmsList.asObservable();
  }

  public setFilmsValue(value: FilmsResponse): any {
    return this.filmsList.next(value);
  }

  public getFilmDetailsObservable(): Observable<Film> {
    return this.filmDetails.asObservable();
  }

  public setFilmDetailsValue(value: Film): any {
    return this.filmDetails.next(value);
  }

  public setFilmsPageById(id: number): void {
    this._getFilmById(id).subscribe((result: Film) => {
      this.setFilmDetailsValue(result);
    });
  }

  public setFilmsPage(url: URL): void {
    this.getPage(url).subscribe((result: any) => {
      if (!!result.count) {
        this.setFilmsValue(result);
      } else {
        this.setFilmDetailsValue(result);
      }
    });
  }

  public getPage(url: URL): Observable<any> { // TODO add CharactersResponse | FilmsResponse type
    return this.http.get(url);
  }
}
