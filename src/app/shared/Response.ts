export interface Response {
    count: number;
    previous: string;
    next: string;
    results: Array<any>;
}
