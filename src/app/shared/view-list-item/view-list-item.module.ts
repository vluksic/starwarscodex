import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewListItemComponent } from './view-list-item.component';
import { MatCardModule } from '@angular/material/card';



@NgModule({
    declarations: [ViewListItemComponent],
    exports: [
        ViewListItemComponent
    ],
    imports: [
        CommonModule,
        MatCardModule
    ]
})
export class ViewListItemModule { }
