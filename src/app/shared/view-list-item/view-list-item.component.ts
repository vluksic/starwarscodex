import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-list-item',
  templateUrl: './view-list-item.component.html',
  styleUrls: ['./view-list-item.component.scss']
})
export class ViewListItemComponent implements OnInit {
  @Input() viewListItemData = undefined;

  constructor() { }

  ngOnInit(): void {
  }

}
