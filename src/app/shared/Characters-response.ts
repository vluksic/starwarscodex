import { Character } from './Character';
import { Response } from './Response';

export interface CharactersResponse extends Response {
    results: Array<Character>;
}
