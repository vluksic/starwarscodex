import { NgModule } from '@angular/core';
import { DataServiceModule } from './data-service/data-service.module';
import { PaginationModule } from './pagination/pagination.module';
import { SearchPipe } from './Search.pipe';
import { PersistenceService } from './persistence/persistence.service';
import { ViewListItemModule } from './view-list-item/view-list-item.module';
import { FilterModule } from './filter/filter.module';



@NgModule({
  declarations: [
      SearchPipe
  ],
  imports: [
      PaginationModule,
      DataServiceModule,
      ViewListItemModule,
      FilterModule
  ],
    exports: [
        PaginationModule,
        DataServiceModule,
        ViewListItemModule,
        FilterModule,
        SearchPipe
    ],
    providers: [
        PersistenceService
    ]
})
export class SharedModule { }
