import { Response } from './Response';
import { Film } from './Film';

export interface FilmsResponse extends Response {
    results: Array<Film>;
}

