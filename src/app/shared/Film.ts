import { URL } from './Types';

export interface Film {
    title: string;
    episode_id: number;
    opening_crawl: string;
    director: string;
    producer: string;
    release_date: string;
    characters: Array<URL>;
    planets: Array<URL>;
    starships: Array<URL>;
    vehicles: Array<URL>;
    species: Array<URL>;
    url: URL;
}
