import { URL } from './Types';

export interface Character {
    birth_year: string;
    eye_color: string; // TODO swap to enum
    films: Array<URL>;
    gender: string; // TODO swap to enum
    hair_color: string; // TODO swap to enum
    height: string;
    homeworld: URL;
    mass: string;
    name: string;
    skin_color: string; // TODO swap to enum
    species: Array<any>;
    starships: Array<URL>;
    vehicles: Array<URL>;
    url: URL;
}
