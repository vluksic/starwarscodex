import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const pathToDefaultView = 'characters';

const routes: Routes = [
  {
    path: '',
    redirectTo: pathToDefaultView,
    pathMatch: 'full'
  },
  // {
  //   path: 'characters',
  //   loadChildren: () => import('./views/characters/characters.module').then(m => m.CharactersModule),
  // },
  // {
  //   path: 'films',
  //   loadChildren: () => import('./views/films/films.module').then(m => m.FilmsModule),
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
